/*! bilisim-enstitusu - v0.0.5 - 2018-12-16
* https://gitlab.com/themesama-education/bilisim-enstitusu#readme
* Copyright (c) 2018 themesama; Licensed CC BY-NC-ND 4.0 */
;(function ( $, window, document, undefined ) {
  'use strict';

  var $window = $(window),
      $document = $(document),
      $body     = $('body'),
      window_height = $window.height(),
      is_screen,
      is_device_mobile = (navigator.userAgent.match(/(Android|iPhone|iPad|iPod|Opera Mini|webOS|BlackBerry|IEMobile)/)) ? true : false;

  /**
   * Exists Element
   *
   */
  $.exists = function(selector) {
    return ($(selector).length > 0);
  };

  /**
   * Viewport Bootstrap
   *
   */
  $.viewportBS = function() {
    if( $window.width() < 768 ) {
      is_screen = 'xs';
    }else if ( $window.width() >= 768 && $window.width() <= 992 ) {
      is_screen = 'sm';
    }else if( $window.width() > 992 && $window.width() <= 1200 ) {
      is_screen = 'md';
    }else  {
      is_screen = 'lg';
    }
  };

  /**
   * startsWith for IE
   *
   */
  if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
      position = position || 0;
      return this.indexOf(searchString, position) === position;
    };
  }

  /**
   * İskeleti duzelt
   *
   */
  $.siteIskelet = function() {
    // site dili
    $body.attr('lang', 'tr');

    // header kenar boslugunu temizle
    $('.gazi-header').removeClass('container-fluid').addClass('container');
    $('.gazi-logo').removeClass('col-lg-offset-1 col-sm-offset-0 col-sm-8 col-lg-6').next().removeClass('col-sm-offset-1 col-lg-offset-1 col-lg-3 col-sm-3');
    
    // gazi portal baglantisi icin yer ac
    $('#nav-col-div').parent().removeClass('col-lg-11').addClass('col-lg-10');
    $('#nav-right-div').removeClass('col-lg-1').addClass('col-lg-2');
    
    // logo ayarla
    $('.gazi-logo').html('<a href="/" id="gazi-site-title"><img src="http://cdn.gazi.edu.tr/gazi2017/img/logo100.png" alt="Gazi Üniversitesi"/><span class="logo-divider"></span><span class="logo-text">'+ $('#gazi-site-title a').text() +'</span></a>');
    $('#nav-right-div').prepend('<a href="http://gazi-universitesi.gazi.edu.tr/" target="_blank" class="gazi-portal">GAZİ PORTAL</a>');

    // menuyu hizalama
    $('#navbar-border > div').wrapAll('<div class="container"><div class="row"></div></div>');

    //icerigi hizalama
    $('#content > .container > div').wrapAll('<div class="row"></div>');

    // mobil menu butonu guzellestir
    $('#menu-lines').addClass('fa fa-bars');

    // mobil header
    $('#navbar-hidden-menu #search-form-mbl').parent().removeClass('col-xs-5').addClass('col-xs-8');
    $('#navbar-hidden-menu .lang').removeClass('col-xs-5 text-right').addClass('col-xs-2');
    if( $('#navbar-hidden-menu .lang > a').html() === "English" ) {
      $('#navbar-hidden-menu .lang > a').html('EN');
    }

    // duyuru ve haber icerigi
    $('[data-wordy-screen-id="view-post"]').removeClass('container-fluid').addClass('container').find('> .info-div').addClass('row');
    $('[data-wordy-screen-id="view-post"] > .info-div .col-lg-offset-1.col-lg-6').removeClass('col-lg-offset-1 col-lg-6').addClass('col-lg-8');
    $('[data-wordy-screen-id="view-post"] > .info-div .col-lg-offset-1.col-lg-3').removeClass('col-lg-offset-1 col-lg-3').addClass('col-lg-4');
  };

  /**
   * Arama ek özellik - site içi arama
   *
   */
  $.aramaEkOzellik = function() {
    $('#search-ul').prepend('<li class="enstitu-arama"><a href="#">Enstitü\'de Ara</a></li>');
    $('.enstitu-arama').click(function(e) {
      e.stopPropagation();
      e.preventDefault();
      var search_text = $('#gazi-search [name="q"]').val();

      $('#gazi-search').prepend('<input type="hidden" value="012149363810515752932:eymqz37-u2m" name="cx">');
      $('#gazi-search').attr('action', 'http://www.google.com/cse');
      $('#gazi-search [name="q"]').val(search_text + ' site:be.gazi.edu.tr');
      $('#gazi-search').submit();
      $('#gazi-search [name="q"]').val(search_text);
    });
  };

  /**
   * 
   *
   */
  $(document).ready( function(){
    // mobilse
    if(is_device_mobile) {
      $body.addClass('touchable-device');
    }

    // bilisim enstitusu ise
    if( $body.data('current-site') === 'be' ) {
      $body.addClass('be');

      //
      $.siteIskelet();

      //
      $.aramaEkOzellik();

      // table responsive
      $('.info-details table').wrap('<div class="table-responsive"></div>');

      // footer
      $('.footer-address').html('<p>Tunus Cad. No: 35 Kavaklıdere Çankaya/ANKARA</p><p>(0 312) 202 3801 / (0 312) 215 5483</p>');
      $('footer a[href=""]').remove();
    }

    //Viewport
    $.viewportBS();

    
  });

  $(window).resize( function(){
    window_height = $window.height();
    $.viewportBS();
  });

  $(window).scroll( function(){
    // do stuff
  });

})( jQuery, window, document );