# Gazi Üniversitesi Bilişim Enstitüsü Mevcut Web Sayfasına Tablet ve Mobil Uyumluluğu Kazandırma
Bu proje Responsive Tasarım tekniği kullanarak mevcut bir web sayfasına ve içeriklerine tablet ve mobil uyumluluğu kazandırmayı amaçlamaktadır. Projenin tüm detayları ve hakları Gazi Üniversitesi Bilişim Enstitüsüne aittir.

## Başlarken
Projede güncel web teknolojilerinin geliştirilmesinde sıklıkla kullanılan NodeJS, NPM, GruntJS, SASS ve JS dillerinden ve paket yönetim araçlarından faydalanılacaktır.

### Ön Gereksinimler
Projede geliştirme yapabilmek için aşağıda belirtilen uygulama ve kütüphanelerin yerel bilgisayarınızda kurulu olması gerekmektedir.

 - [NodeJS](https://nodejs.org/)
 - [NPM](https://www.npmjs.com/)
 - [GruntJS](https://gruntjs.com/)
 - [git](https://git-scm.com/downloads)
 - [Text Editör](https://www.sublimetext.com/)

### Kurulum Aşamaları
Proje için gerekli olan uygulama ve kütüphaneleri aşağıdaki adımları izleyerek edinebilirsiniz.

#### NodeJS Kurulum Detayları
Paket yönetim araçları ile yükleme yapmak için işletim sisteminize uygun olan [yönergeleri](https://nodejs.org/en/download/package-manager/) takip ediniz. NodeJS kurulumunun ardından bilgisayarınızın komut satırına sırası ile belirtilen komutları girerek NodeJS ve NPM kurulumlarının gerçekleştiğini doğrulayabilirsiniz.

```
node -v
npm -v
```

#### GruntJS Kurulumu
GruntJS Javascript tabanlı görev yönetim (task manager) aracıdır. Kütüphanelerimizin derlenmesi aşamasında kendisine tanımlanan otomatik görevleri yerine getirir. NPM paket yönetimi aracılığıyla aşağıdaki komutu bilgisayarınızın komut satırına yazarak kurulumu gerçekleştirebilirsiniz.

```
npm install -g grunt-cli
```

#### Git Kurulumu ve Projenin Yerel Bilgisayara İndirilmesi
Git kurulum işlemlerini [git-scm](https://git-scm.com/downloads) adresinde belirtilen yönergeleri takip ederek gerçekleştirebilirsiniz. Kurulum işleminin ardından aşağıdaki satırı komut satırına yazarak projeyi yerel bilgisayarınıza indirerek geliştirebilirsiniz.

```
git clone git@gitlab.com:themesama-education/bilisim-enstitusu.git
```

Bu işlemin ardından bilisim-enstitusu adlı bir dizin ile ilgili dosyalar bilgisayarınıza indirilecektir. Ardından aşağıdaki komutlar yardımıyla geliştirme kütüphanelerinin kurulumunu gerçekleştirebilirsiniz.

```
cd bilisim-enstitusu
npm install
```

Tebrikler! Kurulum işlemlerini tamamladınız.

## Çalıştırma ve kontrol etme

Komut satırına aşağıda belirtilen komutu yazarak proje dosyalarını takip edebilir ve güncellemeler sonrasında atanan görevlerin çalıştığını gözlemleyebilirsiniz.

```
grunt bilisim-enstitusu
```

## Yapılan Değişiklikleri Görüntüleme

Bu işlemler Chrome ya da Firefox tarayıcılarının desteklemekte olduğu TamperMonkey ya da Greasemonkey eklentilerinin kurulumuyla gerçekleştirilebilmektedir. Bu eklentiler UserScript adı verilen JS dosyalarını belirli sayfalar üzerinde çalıştırarak kullanıcıların yerel değişiklikler oluşturmasına olanak tanımaktadır. Böylece kaynak koduna erişim sağlayamadığınız web sayfaları üzerinde JS vasıtasıyla görsel değişiklikler gerçekleştirebilirsiniz. 

[Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=tr) | [Greasemonkey](https://addons.mozilla.org/tr/firefox/addon/greasemonkey/) eklenti kurulumunun ardından [UserScript.js](https://gitlab.com/themesama-education/bilisim-enstitusu/blob/master/UserScript.js) dosyasında yer alan satırları yeni bir kullanıcı betiği oluşturarak eklemeniz gerekmektedir. Ardından projenin Çalıştırma ve kontrol etme adımlarını gerçekleştiriniz. Artık enstitü web sayfasını ziyaret ederek yapılan değişiklikleri inceleyebilirsiniz.

## Sürümler

Projenin sürümleri [CHANGELOG.md](https://gitlab.com/themesama-education/bilisim-enstitusu/blob/master/CHANGELOG.md) dosyasında yer almaktadır.

## Yazar

* **Safa HALİLOĞLU** - [ThemeSama](https://linkedin.com/in/themesama/)

## Lisans

Bu eser [Creative Commons Atıf-GayriTicari-Türetilemez 4.0 Uluslararası Lisansı](http://creativecommons.org/licenses/by-nc-nd/4.0/) ile lisanslanmıştır.
