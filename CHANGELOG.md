# bilisim-enstitusu projesi değişiklik listesi

0.0.5

- Ana sayfa görsel sorunları giderildi.
- Sayfa içeriği ile ilgili hizalamalar ve geliştirmeler tamamlandı.
- Mobil ve tablet uyumu ile ilgili örnek çalışmalar tamamlandı.
- Ana sayfa slayt gösterisi butonları özelleştirildi.
- Sayfa başlıkları belirgin hale getirildi.
- Ana sayfa da bulunan görsel sorunlar giderildi.
- Hızlı bağlantılar kısmındaki bağlantılar hizalandı.
- Sayfa alt alanı ile ilgili geliştirmeler tamamlandı.
- Anabilim Dalları - YBS sayfası geliştirmelere örnek olması için oluşturuldu.
- Sayfa içi bileşenlerin geliştirilmesi tamamlandı.
- PDF dosyası sunma ile ilgili örnekler oluşturuldu.
- Ana sayfa slaty gösterisi için kaliteli ve modern görseller oluşturuldu.
- Duyuru detayında mobil görünümde sorun çıkaran takvim üzerinde değişiklik yapıldı.
- İskelet yapının bozulmasına neden olan iç sayfalar ve sayfa içi bileşenler düzenlendi.
- Mobil ve tablet uyumlu sekme bileşeni sisteme dahil edildi.
- Bir önceki sürümden kaynaklı ana menü hataları giderildi.

0.0.4

- Arama çubuğu özelleştirmeleri yapıldı. Artık ziyaretçiler site içi arama yapabiliyorlar
- Arama çubuğu tasarımı değiştirildi.
- Ana menü özelleştirmeleri yapıldı. Menüler daha belirgin ve kullanışlı hale getirildi.
- Menu çubuğu yatay ve dikey hizalamaları yapıldı.
- Gazi Üniversitesi ana sayfası bağlantısı ayarlandı.
- Mobil menü butonu daha kaliteli bir sürümü ile değiştirildi.
- Başlık ve içerik ile ilgili hizalamalar yapıldı.
- Slider butonları gizlendi. Üzerine gelindiğinde görünecek şekilde ayarlandı.
- Slider aktif slayt butonunun rengi ayarlandı.
- Ana sayfa başlıkları daha belirgin hale getirildi.
- Ana sayfa mobil içerik kayma sorunları giderildi.
- Hızlı bağlantılar kısmındaki görünüm güzelleştirildi.
- Sayfa altındaki içerik standart yazı tipine döndürüldü.

0.0.3

- Analiz aşamasında logo için yapılan öneriler gerçekleştirildi. Artık logo yalnızca enstitü web sayfasının ana sayfasına yönlendirme yapabilecek.
- Analiz aşamasında sayfa başlığı ile ilgili yapılan önerilerin bir kısmı gerçekleştirildi. Arama butonuna ek özellik daha sonra eklenecek.
- Ana menü ile ilgili özelleştirmeler gerçekleştirildi. Renkler, boşluklar ve hizalamalar ile ilgili geliştirmeler yapıldı.
- Geliştirme ortamı ile ilgili ufak değişiklikler gerçekleştirildi.

0.0.2

- Görev yönetimi ile ilgili düzenlemeler yapıldı
- JS dosyasının hatasız oluşabilmesi için gerekli olan Grunt görevleri ayarlandı
- SASS ile çalışabilmek için gerekli olan ayarlamalar yapıldı
- Proje dizininde yapılan değişikliklerin algılanarak tarayıcının otomatik olarak yenilenmesi sağlandı
- Projenin içerisindeki JS ve CSS dosyalarını canlı ortama aktarabilmesi için webserver olarak çalışması sağlandı
- Yapılan değişikliklerin sisteme yansımasını anlatan içerikler oluşturuldu
- Gerekli denemeler yapıldı

0.0.1

- İlk yükleme