// ==UserScript==
// @name         bilisim-enstitusu
// @namespace    http://be.gazi.edu.tr/
// @version      0.1
// @description  Bilisim Enstitusu Tablet ve Mobil Uyumlulugu!
// @author       themesama
// @match        http://be.gazi.edu.tr/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var livereload_script = document.createElement('script'),
        be_script = document.createElement('script'),
        be_style = document.createElement('link');
    //
    livereload_script.setAttribute('src','//localhost:35729/livereload.js');
    be_script.setAttribute('src', 'http://localhost:9001/js/bilisim-enstitusu.min.js');
    be_style.setAttribute('href', 'http://localhost:9001/css/bilisim-enstitusu.css');
    be_style.setAttribute('rel', 'stylesheet');
    //
    document.head.appendChild(livereload_script);
    document.head.appendChild(be_script);
    document.head.appendChild(be_style);
})();